//
//  AriistoGalleryViewController.h
//  Ariisto
//
//  Created by Kirti Nikam on 24/08/12.
//
//

#import <UIKit/UIKit.h>
#import "AriistoAppDelegate.h"

@interface AriistoGalleryViewController : UIViewController<UIScrollViewDelegate>{
   
    UIScrollView *scrollView;
    UIPageControl *pageControl;
    
    NSMutableArray *viewControllers;
    BOOL pageControlUsed;    
    NSArray *homeImages;
    NSMutableArray *GalleryImages;
}
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIPageControl *pageControl;
@property (nonatomic, strong) NSMutableArray *viewControllers;
- (IBAction)btnBack:(id)sender;


- (IBAction)changePage:(id)sender;
- (void)loadScrollViewWithPage:(int)page ;
- (void)unloadScrollViewWithPage:(int)page ;
@end
