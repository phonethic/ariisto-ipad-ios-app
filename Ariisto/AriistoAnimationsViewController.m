//
//  AriistoAnimationsViewController.m
//  Ariisto
//
//  Created by Kirti Nikam on 26/08/12.
//
//

#import "AriistoAnimationsViewController.h"
#import "AriistoAppDelegate.h"
@interface AriistoAnimationsViewController ()

@end

@implementation AriistoAnimationsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
     [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];
    
    
    NSURL *url = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"animation" ofType:@"mp4"]];
    moviePlayer =  [[MPMoviePlayerController alloc] initWithContentURL:url];
    moviePlayer.view.frame = [[UIScreen mainScreen] applicationFrame];
    [[NSNotificationCenter defaultCenter]
               addObserver:self
               selector:@selector(moviePlayBackDidFinish:)
               name:MPMoviePlayerPlaybackDidFinishNotification
               object:moviePlayer];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moviePlayBackDidFinish:)
                                                 name:MPMoviePlayerDidExitFullscreenNotification object:moviePlayer];
    
    moviePlayer.controlStyle = MPMovieControlStyleNone;
    moviePlayer.shouldAutoplay = YES;
    [moviePlayer setFullscreen:YES animated:YES];
    [self.view addSubview:moviePlayer.view ];
    
    [ARIISTO_APP_DELEGATE playOnVideo];

   // [moviePlayer play];
}

- (void) moviePlayBackDidFinish:(NSNotification*)notification {
     MPMoviePlayerController *player = [notification object];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:MPMoviePlayerDidExitFullscreenNotification
                                                    object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:MPMoviePlayerPlaybackDidFinishNotification
                                                    object:player];
    
    [moviePlayer stop];
    [moviePlayer.view removeFromSuperview];
    [ARIISTO_APP_DELEGATE stopAnimation];
    
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return (interfaceOrientation == UIInterfaceOrientationPortrait);
   
}


@end
