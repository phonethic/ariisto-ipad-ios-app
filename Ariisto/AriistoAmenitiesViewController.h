//
//  AriistoAmenitiesViewController.h
//  Ariisto
//
//  Created by Kirti Nikam on 29/08/12.
//
//

#import <UIKit/UIKit.h>
#import "AriistoAppDelegate.h"
#import "AriistoShowAmenityImageViewController.h"

@interface AriistoAmenitiesViewController : UIViewController<UIScrollViewDelegate>{
    
    NSMutableArray *viewControllers;
    BOOL pageControlUsed;
    int AmenityType;
    
    BOOL iBTNFLAG;

}

@property (strong, nonatomic) IBOutlet UIScrollView *internalScrollView;
@property (strong, nonatomic) IBOutlet UIPageControl *internalPageControl;

@property (strong, nonatomic) IBOutlet UIScrollView *externalScrollView;
@property (strong, nonatomic) IBOutlet UIPageControl *externalPageControl;

@property (strong, nonatomic) IBOutlet UITextView *showTextPopUp;

@property (strong, nonatomic) IBOutlet UIButton *amenitytogglebtn;


@property (nonatomic, strong) NSMutableDictionary *amenitiesdata;

@property (nonatomic, retain) NSMutableArray *viewInternalControllers;
@property (nonatomic, retain) NSMutableArray *viewExternalControllers;

@property (nonatomic, assign) NSInteger AmenityType;

@property (strong, nonatomic) IBOutlet UIButton *ibtn;

//- (IBAction)changePage:(id)sender;
- (void)loadScrollViewWithPage:(int)page type:(int)amtype;
- (void)unloadScrollViewWithPage:(int)page type:(int)amtype;

- (IBAction)btnBack:(id)sender;
- (IBAction)btnToggleAmenities:(id)sender;
- (IBAction)ibtnMethod:(id)sender;

@end
