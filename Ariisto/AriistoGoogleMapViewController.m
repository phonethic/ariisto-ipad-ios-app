//
//  AriistoGoogleMapViewController.m
//  Ariisto
//
//  Created by Kirti Nikam on 29/08/12.
//
//

#import "AriistoGoogleMapViewController.h"

@interface AriistoGoogleMapViewController ()

@end

@implementation AriistoGoogleMapViewController
@synthesize mapView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewWillAppear:(BOOL)animated {
	// hide navigation bar
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
    
    mapView.mapType=MKMapTypeStandard;
    mapView.scrollEnabled=YES;
    mapView.zoomEnabled=YES;
    mapView.delegate=self;
    
   

    MKCoordinateRegion region;
    region.center.latitude = 19.07987899;
	region.center.longitude = 72.8405732;
    
	region.span.longitudeDelta = 0.01;
	region.span.latitudeDelta = 0.01;
    
    //testing with current location span
    //region.center.latitude = 19.131724;
	//region.center.longitude = 72.833892;

    [mapView setRegion:region animated:TRUE];
    annotation=[[AriistoMyAnnotation alloc] initWithCoordinate:region.center title: @"Caesar Road, Jai Bhavani Road," subtitle:@"Amboli, Andheri(W), Mumbai-40054"];
    [mapView addAnnotation:annotation];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [self setMapView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(MKAnnotationView *)mapView:(MKMapView *)aMapView viewForAnnotation:(id)ann {
    
    MKPinAnnotationView *pin = nil;
    
    if (ann != mapView.userLocation) {
        NSString *identifier = @"myPin";
        pin = (MKPinAnnotationView *)
        [aMapView dequeueReusableAnnotationViewWithIdentifier:identifier];
        
        if (pin == nil)
        {
            pin = [[MKPinAnnotationView alloc] initWithAnnotation:ann reuseIdentifier:identifier];
        }
        
        else {
            pin.annotation = ann;
        }
               
        pin.pinColor = MKPinAnnotationColorRed;
        pin.enabled = YES;
        pin.animatesDrop=TRUE;
        pin.canShowCallout=YES;
    }
    else
        [mapView.userLocation setTitle:@"I am here"];
    return pin;
}
- (IBAction)btnBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
