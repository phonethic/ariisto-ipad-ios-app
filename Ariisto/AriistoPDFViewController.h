//
//  AriistoPDFViewController.h
//  Ariisto
//
//  Created by Kirti Nikam on 30/08/12.
//
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "AriistoAppDelegate.h"
#import "ReaderViewController.h"

@interface AriistoPDFViewController : UIViewController <UIWebViewDelegate,NSURLConnectionDelegate,UIAlertViewDelegate,ReaderViewControllerDelegate>
{
    NSURLConnection *conn;
    NSMutableData *webData;
    NSString *filePath;
    float contentSize;    
    
    BOOL FLAG_BACK;
}
@property (strong, nonatomic) IBOutlet UIImageView *man_imageview;
@property (strong, nonatomic) IBOutlet UIView *subView;
@property (strong, nonatomic) IBOutlet UIProgressView *progressView;
@property (strong, nonatomic) IBOutlet UIWebView *webView;
- (IBAction)btnBack:(id)sender;

@end
