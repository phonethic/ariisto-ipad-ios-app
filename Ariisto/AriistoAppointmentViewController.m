//
//  AriistoAppointmentViewController.m
//  Ariisto
//
//  Created by Kirti Nikam on 31/08/12.
//
//

#import "AriistoAppointmentViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface AriistoAppointmentViewController ()

@end

@implementation AriistoAppointmentViewController
@synthesize textFieldName;
@synthesize textFieldEmail;
@synthesize textFieldPhone;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self roundViewWithBorder:textFieldName];
    [self roundViewWithBorder:textFieldEmail];
    [self roundViewWithBorder:textFieldPhone];
    
     UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    gestureRecognizer.cancelsTouchesInView=NO;
    [self.view addGestureRecognizer:gestureRecognizer];
}

- (void) hideKeyboard {
   [self.view endEditing:YES];
}

-(void) roundViewWithBorder:(UITextField *)text
{
    text.layer.cornerRadius = 10;
    text.clipsToBounds = YES;
    text.layer.borderColor = [UIColor lightGrayColor].CGColor;
    text.layer.borderWidth = 1.0;
}
- (void)viewDidUnload
{
    [self setTextFieldName:nil];
    [self setTextFieldEmail:nil];
    [self setTextFieldPhone:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return (interfaceOrientation == UIInterfaceOrientationPortrait);
    
}

- (IBAction)btnBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];

}

- (IBAction)btnSend:(id)sender {
    if ([textFieldName.text isEqualToString:@""]) {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Warning" message:@"Please Enter your name." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
   else if (![self validateEmail:textFieldEmail.text]) {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Warning" message:@"Please Enter correct email address." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }else if (![self validatePhoneNumber:textFieldPhone.text])
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Warning" message:@"Please Enter correct contact no.(Start with +91)" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    else{
        NSLog(@"success");
        MFMailComposeViewController *picker=[[MFMailComposeViewController alloc]init];
        picker.mailComposeDelegate=self;
        
        NSString *msgBody=[NSString stringWithFormat:@"Name:  %@ \n\nEmail:  %@ \n\n Phone:  %@",textFieldName.text,textFieldEmail.text,textFieldPhone.text];
        
        [picker setToRecipients:[NSArray arrayWithObject:@"sales@ariisto.com"]];
        [picker setMessageBody:msgBody isHTML:NO];
        [picker setModalPresentationStyle:UIModalPresentationFormSheet];
        [picker setModalTransitionStyle:UIModalTransitionStyleFlipHorizontal];
        [self presentModalViewController:picker animated:YES];
    }
}



-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    NSInteger nextTag = textField.tag + 1;
    UIResponder *next=[textField.superview viewWithTag:nextTag];
    if (next) {
        [next becomeFirstResponder];
    }
    else
     [textField resignFirstResponder];
    return NO;
}

-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
    
    [controller dismissModalViewControllerAnimated:YES];
    textFieldName.text=@"";
    textFieldEmail.text=@"";
    textFieldPhone.text=@"";
}

-(BOOL)validateEmail:(NSString *) emailString {
    
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:emailString];
}

-(BOOL)validatePhoneNumber:(NSString *) phoneString {
    NSString *phoneRegex=@"[+]+91[0-9]{10}";
   // NSString *phoneRegex=@"[0-9]{10}";
    NSPredicate *noTest=[NSPredicate predicateWithFormat:@"SELF MATCHES %@",phoneRegex];
    return [noTest evaluateWithObject:phoneString];    
    
}
@end
