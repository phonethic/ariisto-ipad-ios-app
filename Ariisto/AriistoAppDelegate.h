//
//  AriistoAppDelegate.h
//  Ariisto
//
//  Created by Kirti Nikam on 24/08/12.
//
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "Reachability.h"
#import "PanoormaAnimation.h"

@class AriistoViewController;
@class AriistoAnimationsViewController;

//#define NO_STARTING_ANIMATION
#define ARIISTO_APP_DELEGATE (AriistoAppDelegate *)[[UIApplication sharedApplication] delegate]

@interface AriistoAppDelegate : UIResponder <UIApplicationDelegate,UITabBarControllerDelegate,AVAudioPlayerDelegate>{
	AVAudioPlayer *scrollAudioPlayer;
    AVAudioPlayer *iButtonAudioPlayer;
    AVAudioPlayer *clickAudioPlayer;
    AVAudioPlayer *videoAudioPlayer;
    AVAudioPlayer *openDoorAudioPlayer;
    
    Reachability *internetReach;
    Boolean networkavailable;
    
    PanoormaAnimation *pancontroller;
}


@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) AriistoViewController *viewController;
@property (strong, nonatomic) UITabBarController *tabBarController;
@property (strong, nonatomic) AriistoAnimationsViewController *animationviewController;
@property(nonatomic,readonly) Boolean networkavailable;


-(void)playOnScroll;
-(void)playOniButton;
-(void)playOnDropDown;
-(void)playOnVideo;
-(void)playOnOpenDoor;

-(void)stopAnimation;
-(void)stopPanorama;
@end
