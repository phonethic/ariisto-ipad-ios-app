//
//  AriistoGoogleMapViewController.h
//  Ariisto
//
//  Created by Kirti Nikam on 29/08/12.
//
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "AriistoMyAnnotation.h"


@interface AriistoGoogleMapViewController : UIViewController<MKMapViewDelegate>
{
    AriistoMyAnnotation *annotation;
}
@property (strong, nonatomic) IBOutlet MKMapView *mapView;
- (IBAction)btnBack:(id)sender;

@end
