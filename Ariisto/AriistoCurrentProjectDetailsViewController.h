//
//  AriistoCurrentProjectDetailsViewController.h
//  Ariisto
//
//  Created by Kirti Nikam on 31/08/12.
//
//

#import <UIKit/UIKit.h>
#import "AriistoAppDelegate.h"

#define _GRID_VIEW_ 3X3

@interface AriistoCurrentProjectDetailsViewController : UIViewController<UIScrollViewDelegate>{

    BOOL pageControlUsed;
    BOOL BUTTONFLAG;
    BOOL LOADFLAG;
}

@property (strong, nonatomic) IBOutlet UIImageView *headerView;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIPageControl *pageControl1;
@property (strong, nonatomic) IBOutlet UIImageView *imageView;
@property (strong, nonatomic) IBOutlet UIButton *arrowBtn;

- (IBAction)arrowBtnPressed:(id)sender;

- (IBAction)btnActionMethod:(id)sender;
- (IBAction)btnBack:(id)sender;
- (IBAction)pageChange:(id)sender;

@end
