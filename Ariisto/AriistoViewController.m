//
//  AriistoViewController.m
//  Ariisto
//
//  Created by Kirti Nikam on 24/08/12.
//
//

#import "AriistoViewController.h"

@interface AriistoViewController ()

@end

@implementation AriistoViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return (interfaceOrientation == UIInterfaceOrientationPortrait);
    
}

@end
