//
//  AriistoElevationViewController.h
//  Ariisto
//
//  Created by Kirti Nikam on 28/08/12.
//
//

#import <UIKit/UIKit.h>

@interface AriistoElevationViewController : UIViewController<UIScrollViewDelegate>
{
    BOOL toggle;
}
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) UIImageView *imageView;
- (IBAction)btnBack:(id)sender;
- (IBAction)btnToggle:(id)sender;
@end
