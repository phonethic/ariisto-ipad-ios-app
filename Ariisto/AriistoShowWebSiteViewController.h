//
//  AriistoShowWebSiteViewController.h
//  Ariisto
//
//  Created by Kirti Nikam on 31/08/12.
//
//

#import <UIKit/UIKit.h>

@interface AriistoShowWebSiteViewController : UIViewController< UIWebViewDelegate>
- (IBAction)btnBack:(id)sender;
@property (strong, nonatomic) IBOutlet UIWebView *webView;
@end
