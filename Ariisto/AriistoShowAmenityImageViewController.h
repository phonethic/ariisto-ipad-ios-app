//
//  AriistoShowAmenityImageViewController.h
//  Ariisto
//
//  Created by Kirti Nikam on 04/09/12.
//
//

#import <UIKit/UIKit.h>

@interface AriistoShowAmenityImageViewController : UIViewController{
    NSString *imageNameString;

}
@property (strong, nonatomic) IBOutlet UIImageView *imageView;
- (IBAction)btnBack:(id)sender;
- (id)initWithImageName:(NSString *)name;
@end
