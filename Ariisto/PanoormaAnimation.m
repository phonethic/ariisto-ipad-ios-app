//
//  PanoormaAnimation.m
//  ipadpan
//
//  Created by Sagar Mody on 09/09/12.
//  Copyright (c) 2012 Sagar Mody. All rights reserved.
//

#import "PanoormaAnimation.h"
#import "AriistoAppDelegate.h"

@interface PanoormaAnimation ()

@end

@implementation PanoormaAnimation
@synthesize webView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [webView loadRequest:[NSURLRequest requestWithURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"index" ofType:@"html" inDirectory:@"vr5_script"]isDirectory:NO]]];
	[webView setUserInteractionEnabled:YES];
	[self.navigationController setNavigationBarHidden:YES];
    [super viewDidLoad];
}

- (void)viewDidUnload
{
    [self setWebView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


- (IBAction)btnClicked:(id)sender {
    NSLog(@"panoorma:clicked");
   [ARIISTO_APP_DELEGATE stopPanorama];

}
@end
