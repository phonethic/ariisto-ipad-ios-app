//
//  AriistoAppDelegate.m
//  Ariisto
//
//  Created by Kirti Nikam on 24/08/12.
//
//

#import "AriistoAppDelegate.h"

#import "AriistoViewController.h"

#import "AriistoHomeViewController.h"
#import "AriistoProjectsViewController.h"
#import "AriistoContactsViewController.h"
#import "AriistoProjectViewController.h"
#import "AriistoAnimationsViewController.h"


@implementation AriistoAppDelegate
@synthesize tabBarController=_tabBarController;
@synthesize animationviewController;
@synthesize networkavailable;

-(void)playOnScroll{
    
	
	if (scrollAudioPlayer == nil)
		{
            NSURL *url = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"photo_slide_sound" ofType:@"mp3"]];
            scrollAudioPlayer= [[AVAudioPlayer alloc] initWithContentsOfURL:url error:nil];
            [scrollAudioPlayer setDelegate:self];
            [scrollAudioPlayer setVolume:1.0];
        }	
		[scrollAudioPlayer play];

}
-(void)playOniButton{
    
	
	if (iButtonAudioPlayer == nil)
    {
        
        NSURL *url = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"i_button_sound" ofType:@"mp3"]];
        iButtonAudioPlayer= [[AVAudioPlayer alloc] initWithContentsOfURL:url error:nil];
        [iButtonAudioPlayer setDelegate:self];
        [iButtonAudioPlayer setVolume:1.0];
    }
    [iButtonAudioPlayer play];
    
}
-(void)playOnDropDown{
    
	
	if (clickAudioPlayer == nil)
    {
        
        NSURL *url = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"drop_down_menu_sound" ofType:@"mp3"]];
        clickAudioPlayer= [[AVAudioPlayer alloc] initWithContentsOfURL:url error:nil];
        [clickAudioPlayer setDelegate:self];
        [clickAudioPlayer setVolume:1.0];
    }
    [clickAudioPlayer play];
    
}
-(void)playOnVideo{
    
	
	if (videoAudioPlayer == nil)
    {
        
        NSURL *url = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"video_audio" ofType:@"mp3"]];
        videoAudioPlayer= [[AVAudioPlayer alloc] initWithContentsOfURL:url error:nil];
        [videoAudioPlayer setDelegate:self];
        [videoAudioPlayer setVolume:1.0];
    }
    [videoAudioPlayer play];
    
}
-(void)playOnOpenDoor{
	
	if (openDoorAudioPlayer == nil)
    {
        
        NSURL *url = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"folding_door_open" ofType:@"mp3"]];
        openDoorAudioPlayer= [[AVAudioPlayer alloc] initWithContentsOfURL:url error:nil];
        [openDoorAudioPlayer setDelegate:self];
        [openDoorAudioPlayer setVolume:1.0];
    }
    [openDoorAudioPlayer play];
    
}
-(void)stopAnimation
{
    
    [animationviewController.view removeFromSuperview];    
    [self showPanorama];
}

-(void) showPanorama{

    pancontroller = [[PanoormaAnimation alloc]initWithNibName:@"PanoormaAnimation" bundle:nil];
    [self.window addSubview:pancontroller.view];
}

-(void)stopPanorama{
    [pancontroller.view removeFromSuperview];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
    
    [UIView transitionWithView:self.window duration:0.8  options:UIViewAnimationOptionTransitionFlipFromLeft
                     animations:^(void) {
                         BOOL oldState = [UIView areAnimationsEnabled];
                         [UIView setAnimationsEnabled:NO];
                         self.window.rootViewController = self.tabBarController;
                         [UIView setAnimationsEnabled:oldState];
                     }
                     completion:nil];
    self.tabBarController.selectedIndex = 1;

}
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    UIViewController *homeViewController = [[AriistoHomeViewController alloc] initWithNibName:@"AriistoHomeViewController" bundle:nil] ;
    UIViewController *contactsViewController = [[AriistoContactsViewController alloc] initWithNibName:@"AriistoContactsViewController" bundle:nil] ;
    UIViewController *currentprojectViewController = [[AriistoProjectViewController alloc] initWithNibName:@"AriistoProjectViewController" bundle:nil] ;
    UIViewController *projectsViewController = [[AriistoProjectsViewController alloc] initWithNibName:@"AriistoProjectsViewController" bundle:nil] ;
    
    UINavigationController *homenavController=[[UINavigationController alloc] initWithRootViewController:homeViewController];
    // homenavController.tabBarItem.title=@"Home";
    //  homenavController.tabBarItem.image=[UIImage imageNamed:@"first"];
    
    UINavigationController *currentprojectnavController=[[UINavigationController alloc] initWithRootViewController:currentprojectViewController];
    // currentnavController.tabBarItem.title=@"Current";
    
    UINavigationController *projectsnavController=[[UINavigationController alloc] initWithRootViewController:projectsViewController];
    //  projectsViewController.tabBarItem.title=@"Projects";
    
    UINavigationController *contactsnavController=[[UINavigationController alloc] initWithRootViewController:contactsViewController];
    //   contactsnavController.tabBarItem.title=@"Contacts";
    
    NSMutableArray *viewControlllers=[[NSMutableArray alloc] init];
    [viewControlllers addObject:homenavController];      
    [viewControlllers addObject:currentprojectnavController];
    [viewControlllers addObject:projectsnavController];
    [viewControlllers addObject:contactsnavController];
    
    
    self.tabBarController = [[UITabBarController alloc] init];    
    self.tabBarController.viewControllers = viewControlllers;    
    self.tabBarController.delegate = self;
    
   if ([[[UIDevice currentDevice] systemVersion] floatValue] > 4.9) {
        //iOS 5
        for(int iLoop = 0; iLoop < [self.tabBarController.viewControllers count]; iLoop++)
        {
            UIImageView *imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"tabIconiPad_%i_gray.png",iLoop + 1]]];
            [imgView setFrame:CGRectMake(136+iLoop * 120, 0, 120, 50)];
            [self.tabBarController.tabBar insertSubview:imgView atIndex:1];
            imgView.tag = 1000;
            
        }
    }
    else {
        //iOS 4.whatever and below
        for(int iLoop = 0; iLoop < [self.tabBarController.viewControllers count]; iLoop++)
        {
            UIImageView *imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"tabIconiPad_%i_gray.png",iLoop + 1]]];
            [imgView setFrame:CGRectMake(136+iLoop * 120, 0, 120, 50)];
            [self.tabBarController.tabBar insertSubview:imgView atIndex:0];
            imgView.tag = 1000;
            
        }
    }

    UIImageView *imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"tabIconiPad_%i.png",2]]];
	[imgView setFrame:CGRectMake(256, 0,120, 50)];
	[self.tabBarController.tabBar addSubview:imgView];

    AriistoAnimationsViewController *tempviewController = [[AriistoAnimationsViewController alloc] initWithNibName:@"AriistoAnimationsViewController" bundle:nil];
    self.animationviewController = tempviewController;

#ifdef NO_STARTING_ANIMATION
    self.window.rootViewController = self.tabBarController;
    self.tabBarController.selectedIndex = 1;
#else
    [self.window addSubview:animationviewController.view];
#endif
    [self startCheckNetwork];
    [self.window makeKeyAndVisible];
    return YES;
}
#pragma - TabBar delegate
-(void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController{
    
	for (UIView *view in tabBarController.tabBar.subviews){
		
		if([view isKindOfClass:[UIImageView class]])
		{
			if(view.tag != 1000)
				[view removeFromSuperview];
		}
	}
    
    UIImageView *imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"tabIconiPad_%i.png",self.tabBarController.selectedIndex+1]]];
	[self.tabBarController.tabBar setBackgroundColor:[UIColor blackColor]];
    [imgView setFrame:CGRectMake(136+self.tabBarController.selectedIndex * 120, 0, 120, 50)];
    [self.tabBarController.tabBar addSubview:imgView];
    
}
-(void) startCheckNetwork
{
    
	[[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(reachabilityChanged:) name: kReachabilityChangedNotification object: nil];
	
    internetReach = [Reachability reachabilityForInternetConnection] ;
	[internetReach startNotifier];
    [self updateReachabitlityFlag:internetReach];
    
}

-(void) stopCheckNetwork
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kReachabilityChangedNotification object:nil];
   // [internetReach release];
	internetReach = nil;
}

- (void)reachabilityChanged:(NSNotification *)note
{
	Reachability* curReach = [note object];
	NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
	[self updateReachabilityStatus: curReach];
}

- (void) updateReachabilityStatus: (Reachability*) curReach
{
	if(curReach == internetReach)
	{
		[self updateReachabitlityFlag: curReach];
    }
}

- (void) updateReachabitlityFlag: (Reachability*) curReach
{
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    BOOL connectionRequired=[curReach connectionRequired];
    
    switch (netStatus)
    {
        case NotReachable:
        {
            networkavailable =  NO;
            break;
        }
            
        case ReachableViaWWAN:
        {
			if(connectionRequired==NO)
			{
                networkavailable = YES;
			}
			else
			{
				//this is for invoking internet library
				NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:@"maps.google.com"] cachePolicy:NO timeoutInterval:15.0] ;
				
				NSURLConnection *theConnection = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self];
				if (theConnection) {
					connectionRequired	= NO;
				}
				else {
				}
				
//[theConnection autorelease];
				
			}
			break;
        }
        case ReachableViaWiFi:
        {
            if(connectionRequired==NO)
            {
                networkavailable = YES;
            }
            break;
		}
    }
    if(connectionRequired)
    {
		networkavailable = NO;
    }
    NSLog(@"network status = %d",networkavailable);
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
