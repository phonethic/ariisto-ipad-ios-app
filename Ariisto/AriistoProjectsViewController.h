//
//  AriistoProjectsViewController.h
//  Ariisto
//
//  Created by Kirti Nikam on 31/08/12.
//
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>

#import "AriistoAppDelegate.h"
#import "ImageViewController.h"
#import "AriistoProjectObject.h"

@interface AriistoProjectsViewController : UIViewController<NSXMLParserDelegate,UIScrollViewDelegate,NSURLConnectionDelegate>{
    
    NSMutableArray *viewControllers;
    BOOL pageControlUsed;
    
    NSArray *imagesArray;
//    NSArray *comingsoonArray;
//    NSDictionary *linkArray;
    
    NSXMLParser *xmlParser;
    BOOL elementFound;
    
    NSMutableData *webData;
	NSURLConnection *conn;
    AriistoProjectObject *tempObj;

    
}
@property (nonatomic, strong) NSMutableArray *projectData;
@property (nonatomic, copy) NSString *itunelink;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *statusIndicator;

@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIPageControl *pageControl;
@property (nonatomic, strong) NSMutableArray *viewControllers;


@property (strong, nonatomic) IBOutlet UIButton *btnOperation;


- (IBAction)changePage:(id)sender;
- (void)loadScrollViewWithPage:(int)page ;
- (void)unloadScrollViewWithPage:(int)page ;
- (IBAction)btnOperationMethod:(id)sender;
-(void)readDataFromFile;
-(void)reloadData;
@end
