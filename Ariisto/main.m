//
//  main.m
//  Ariisto
//
//  Created by Kirti Nikam on 24/08/12.
//
//

#import <UIKit/UIKit.h>

#import "AriistoAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AriistoAppDelegate class]));
    }
}
