//
//  AriistoCurrentProjectDetailsViewController.m
//  Ariisto
//
//  Created by Kirti Nikam on 31/08/12.
//
//

#import "AriistoCurrentProjectDetailsViewController.h"
#import "AriistoLocationViewController.h"
#import "AriistoGoogleMapViewController.h"
#import "AriistoElevationViewController.h"
#import "AriistoLayoutPlanViewController.h"
#import "AriistoFloorPlanViewController.h"
#import "AriistoAmenitiesViewController.h"
#import "AriistoPDFViewController.h"
#import "AriistoAppointmentViewController.h"
#import "AriistoGalleryViewController.h"
#import "AriistoContactusGridViewController.h"

@interface AriistoCurrentProjectDetailsViewController ()

@end

@implementation AriistoCurrentProjectDetailsViewController
@synthesize headerView;
@synthesize scrollView;
@synthesize pageControl1;
@synthesize imageView;
@synthesize arrowBtn;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewWillAppear:(BOOL)animated
{
   // NSLog(@"in view will appear %d",LOADFLAG);

    [super viewWillAppear:animated];
    if (!LOADFLAG) {
#ifdef _GRID_VIEW_
        scrollView.frame = CGRectMake(scrollView.frame.origin.x, -(scrollView.frame.size.height-headerView.frame.size.height), scrollView.frame.size.width,scrollView.frame.size.height);
        arrowBtn.frame=CGRectMake(arrowBtn.frame.origin.x, headerView.frame.size.height, arrowBtn.frame.size.width, arrowBtn.frame.size.height);
#else
        scrollView.frame = CGRectMake(scrollView.frame.origin.x, -(scrollView.frame.size.height-headerView.frame.size.height*2), scrollView.frame.size.width,scrollView.frame.size.height-headerView.frame.size.height);
        arrowBtn.frame=CGRectMake(arrowBtn.frame.origin.x, headerView.frame.size.height, arrowBtn.frame.size.width, arrowBtn.frame.size.height);
#endif
        LOADFLAG=true;
    }

 //  [scrollView bringSubviewToFront:pagecontrol];
    
}


- (void)viewDidLoad
{
    
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:YES];

    // Do any additional setup after loading the view from its nib.
    NSArray *buttonImages=[[NSArray alloc] initWithObjects:@"Location",@"Googlemap",@"Elevation",@"LayoutPlan",@"FloorPlan",@"Amenities",@"Gallery",@"PDF",@"Appointments",@"ContactUs", nil ];
    
	[scrollView setBackgroundColor:[UIColor clearColor]];
	[scrollView setCanCancelContentTouches:NO];
	scrollView.indicatorStyle = UIScrollViewIndicatorStyleWhite;
	scrollView.clipsToBounds = YES;		// default is NO, we want to restrict drawing within our scrollview
	scrollView.scrollEnabled = YES;
    scrollView.bounces = NO;
    scrollView.showsVerticalScrollIndicator = NO;
    scrollView.directionalLockEnabled = YES;
    scrollView.delegate = self;
    scrollView.pagingEnabled = YES;
    
#ifdef _GRID_VIEW_
	scrollView.frame = CGRectMake(scrollView.frame.origin.x, -(scrollView.frame.size.height-headerView.frame.size.height), scrollView.frame.size.width,scrollView.frame.size.height);
    arrowBtn.frame=CGRectMake(arrowBtn.frame.origin.x, headerView.frame.size.height, arrowBtn.frame.size.width, arrowBtn.frame.size.height);
#else
    scrollView.frame = CGRectMake(scrollView.frame.origin.x, -(scrollView.frame.size.height-headerView.frame.size.height*2), scrollView.frame.size.width,scrollView.frame.size.height-headerView.frame.size.height);
    arrowBtn.frame=CGRectMake(arrowBtn.frame.origin.x, headerView.frame.size.height, arrowBtn.frame.size.width, arrowBtn.frame.size.height);
#endif
    
	
	
	// load all the images from our bundle and add them to the scroll view
    int numberOfbuttons=[buttonImages count];
    double numberOfbuttonsperpage=9;
    int indexofButtonImage=0;
    
    int pageCount = (int)ceil(numberOfbuttons/numberOfbuttonsperpage);
    NSLog(@"page count = %d ", pageCount);
    
    pageControl1.numberOfPages = pageCount;
    pageControl1.currentPage = 0;
    pageControl1.hidden = TRUE;

	for (int i = 1; i <= pageCount; i++)
	{
		int tag = i * 100;
        
        UIView *view=[[UIView alloc] init];
		CGRect rect;
        view.backgroundColor=[UIColor clearColor];
        
		rect.size.height = scrollView.frame.size.height;
		rect.size.width = scrollView.frame.size.width;
		view.frame = rect;
		view.tag = i;	// tag our images for later use when we place them in serial fashion
		
        UIImageView *img = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"gray_background.png"]];
        img.frame = view.frame;
        img.alpha = 0.9;
        [view addSubview:img];
        //NSLog(@"x=%f y=%f w=%f h=%f ",scrollView.frame.origin.x,scrollView.frame.origin.y,scrollView.frame.size.width,scrollView.frame.size.height );
        
#ifdef _GRID_VIEW_
        int X=90,Y=80;
        int Hspace=53;
        int Vspace=50;
#else
        int X=40,Y=50;
        int Hspace=25;
        int Vspace=25;
#endif
        
        int width=120;
        int height=120;
        int labelHeight=20;
      //  int labelWidth=110;
        UIButton *button;
      //  UILabel *label;
        
        CGRect frame;
        for (int b=0;b<numberOfbuttonsperpage; b++) {
            
            
            frame = CGRectMake(X, Y, width, height);
            button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.frame = frame;
            button.backgroundColor=[UIColor clearColor];
            [button setTitle:[buttonImages objectAtIndex:indexofButtonImage+b] forState:UIControlStateNormal];
            // [button setTitle:[NSString stringWithFormat:@"button%d",b] forState:UIControlStateNormal];
            // [button setImage:[UIImage imageNamed:[buttonImages objectAtIndex:indexofButtonImage+b]] forState:UIControlStateNormal];
            UIImage *image =[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:[buttonImages objectAtIndex:indexofButtonImage+b] ofType:@"png" ]];
            [button setImage:image forState:UIControlStateNormal];
            button.tag = tag+b;
            [button addTarget:self
                       action:@selector(btnActionMethod:)forControlEvents:UIControlEventTouchUpInside];
            
            
            
           /* frame = CGRectMake(X, Y+height, labelWidth, labelHeight);
            label = [[UILabel alloc]initWithFrame:frame];
            label.backgroundColor=[UIColor clearColor];
            label.text=[buttonImages objectAtIndex:indexofButtonImage+b];
            label.textAlignment=UITextAlignmentCenter;
            label.textColor = [UIColor whiteColor];
            label.font=[UIFont boldSystemFontOfSize:15];*/
            
            [view addSubview:button];
            //[view addSubview:label];
            
            button=nil;
           // label=nil;
            int NextX = X+width+Hspace;
            //NSLog(@"NextX %d %f",NextX,view.frame.size.width-100);
            
            if (NextX < view.frame.size.width-60)
            {
                X=NextX;
            }
            else
            {
#ifdef _GRID_VIEW_
                X=90;
#else
                X=40;
#endif
                Y=Y+height+labelHeight+Vspace;
            }
            
        }
        
        [scrollView addSubview:view];
        indexofButtonImage=numberOfbuttonsperpage;
        numberOfbuttonsperpage=numberOfbuttons-numberOfbuttonsperpage;
	}
    
    [self layoutScrollImages:pageCount];
    [self performSelector:@selector(arrowBtnPressed:) withObject:nil afterDelay:0.2];
}

- (void)layoutScrollImages:(int)count
{
	UIView *view = nil;
	NSArray *subviews = [scrollView subviews];
    
	// reposition all subviews in a horizontal serial fashion
	CGFloat curXLoc = 0;
	for (view in subviews)
	{
		if ([view isKindOfClass:[UIView class]] && view.tag > 0)
		{
			CGRect frame = view.frame;
			frame.origin = CGPointMake(curXLoc, 0);
			view.frame = frame;
			
			curXLoc += (scrollView.frame.size.width);
		}
	}
	
	// set the content size so it can be scrollable
	[scrollView setContentSize:CGSizeMake((count * scrollView.frame.size.width), scrollView.frame.size.height)];
}

- (void)scrollViewDidScroll:(UIScrollView *)sender
{
    
    if (pageControlUsed)
    {
        // do nothing - the scroll was initiated from the page control, not the user dragging
        return;
    }
    CGFloat pageWidth = self.scrollView.frame.size.width;
    int page = floor((self.scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    if(pageControl1.currentPage != page) {
        pageControl1.currentPage = page;
    }
}

// At the begin of scroll dragging, reset the boolean used when scrolls originate from the UIPageControl
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    pageControlUsed = NO;
}

// At the end of scroll animation, reset the boolean used when scrolls originate from the UIPageControl
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    pageControlUsed = NO;
}

- (void)viewDidUnload
{
    [self setScrollView:nil];
    [self setPageControl1:nil];
    [self setImageView:nil];
    [self setArrowBtn:nil];
    [self setHeaderView:nil];
    [super viewDidUnload];
   
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (IBAction)arrowBtnPressed:(id)sender {
    //UIButton *btn=(UIButton *)sender;
    //NSLog(@"You clicked %d",btn.tag);
   // NSLog(@"BUTTONFLAG %d",BUTTONFLAG);
    [ARIISTO_APP_DELEGATE playOnOpenDoor];
    
    if (!BUTTONFLAG) {
        [arrowBtn setImage:[UIImage imageNamed:@"arrow_down.png"] forState:UIControlStateNormal];
        [UIView beginAnimations:@"Drop_Down" context:nil];
        [UIView setAnimationDuration:1.5];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDidStopSelector:@selector(transitionDidStop:finished:context:)];
        scrollView.frame = CGRectMake(scrollView.frame.origin.x, headerView.frame.size.height, scrollView.frame.size.width,scrollView.frame.size.height);
        arrowBtn.frame=CGRectMake(arrowBtn.frame.origin.x, scrollView.frame.size.height+headerView.frame.size.height, arrowBtn.frame.size.width, arrowBtn.frame.size.height);
        [UIView commitAnimations];
        
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.5];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft forView:arrowBtn cache:YES];
        [UIView commitAnimations];
        BUTTONFLAG=TRUE;
        
    }else{
        pageControl1.hidden = TRUE;
        [UIView beginAnimations:@"Drop_Up" context:nil];
        [UIView setAnimationDuration:1.5];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDidStopSelector:@selector(transitionDidStop:finished:context:)];
        scrollView.frame = CGRectMake(scrollView.frame.origin.x, -(scrollView.frame.size.height-headerView.frame.size.height), scrollView.frame.size.width,scrollView.frame.size.height);
        arrowBtn.frame=CGRectMake(arrowBtn.frame.origin.x, headerView.frame.size.height, arrowBtn.frame.size.width, arrowBtn.frame.size.height);
        [UIView commitAnimations];
        BUTTONFLAG=FALSE;
        
    }
    
}

- (void)transitionDidStop:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context
{
    if ([animationID isEqualToString:@"Drop_Up"])
    {
        [arrowBtn setImage:[UIImage imageNamed:@"arrow_up.png"] forState:UIControlStateNormal];
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.5];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight forView:arrowBtn cache:YES];
        [UIView commitAnimations];
    } else {
        pageControl1.hidden = FALSE;
    }
}



- (IBAction)btnActionMethod:(id)sender {
    UIButton *button=(UIButton *)sender;
    NSLog(@"Button %@ is clicked",[button titleForState:UIControlStateNormal]);
   
    NSString *btnTitle=[button titleForState:UIControlStateNormal];
    
    if ([btnTitle isEqualToString:@"Location"]) {
        
        UIViewController *locationMapController = [[AriistoLocationViewController alloc] initWithNibName:@"AriistoLocationViewController" bundle:nil] ;
        [self.navigationController pushViewController:locationMapController animated:YES];
    }
    else if ([btnTitle isEqualToString:@"Googlemap"]) {
        
        UIViewController *googleMapController = [[AriistoGoogleMapViewController alloc] initWithNibName:@"AriistoGoogleMapViewController" bundle:nil] ;
        [self.navigationController pushViewController:googleMapController animated:YES];
    }
    else if ([btnTitle isEqualToString:@"Elevation"]) {
        
        UIViewController *elevationController = [[AriistoElevationViewController alloc] initWithNibName:@"AriistoElevationViewController" bundle:nil] ;
        [self.navigationController pushViewController:elevationController animated:YES];
    }
    else if ([btnTitle isEqualToString:@"LayoutPlan"]) {
        
        UIViewController *layoutPlanController = [[AriistoLayoutPlanViewController alloc] initWithNibName:@"AriistoLayoutPlanViewController" bundle:nil] ;
        [self.navigationController pushViewController:layoutPlanController animated:YES];
    }
    else if ([btnTitle isEqualToString:@"FloorPlan"]) {
        
        UIViewController *floorPlanController = [[AriistoFloorPlanViewController alloc] initWithNibName:@"AriistoFloorPlanViewController" bundle:nil] ;
        [self.navigationController pushViewController:floorPlanController animated:YES];
    }
    else if ([btnTitle isEqualToString:@"Amenities"]) {
        
        UIViewController *amenitiesViewController = [[AriistoAmenitiesViewController alloc] initWithNibName:@"AriistoAmenitiesViewController" bundle:nil] ;
        [self.navigationController pushViewController:amenitiesViewController animated:YES];
    }
    else if ([btnTitle isEqualToString:@"Gallery"]) {
        
        UIViewController *galleryController = [[AriistoGalleryViewController alloc] initWithNibName:@"AriistoGalleryViewController" bundle:nil] ;
        [self.navigationController pushViewController:galleryController animated:YES];

    }
    else if ([btnTitle isEqualToString:@"PDF"]) {
        
        UIViewController *pdfviewController = [[AriistoPDFViewController alloc] initWithNibName:@"AriistoPDFViewController" bundle:nil] ;
        [self.navigationController pushViewController:pdfviewController animated:YES];
    }
    else if ([btnTitle isEqualToString:@"Appointments"]) {
        
        UIViewController *appointmentController = [[AriistoAppointmentViewController alloc] initWithNibName:@"AriistoAppointmentViewController" bundle:nil] ;
        [self.navigationController pushViewController:appointmentController animated:YES];
    }
    else if ([btnTitle isEqualToString:@"ContactUs"]) {
        
        UIViewController *contactusController = [[AriistoContactusGridViewController alloc] initWithNibName:@"AriistoContactusGridViewController" bundle:nil] ;
        [self.navigationController pushViewController:contactusController animated:YES];
    }
    else { }
}

- (IBAction)btnBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];

}

- (IBAction)pageChange:(id)sender {
    int page = pageControl1.currentPage;
	
	
	// update the scroll view to the appropriate page
    CGRect frame = self.scrollView.frame;
    frame.origin.x = frame.size.width * page;
    frame.origin.y = 0;
    
    
    [UIView animateWithDuration:0.5
                          delay:0
                        options:UIViewAnimationOptionCurveLinear
                     animations:^{ [scrollView scrollRectToVisible:frame animated:NO]; }
                     completion:NULL];
    
    //[scrollView scrollRectToVisible:frame animated:YES];
    
	// Set the boolean used when scrolls originate from the UIPageControl. See scrollViewDidScroll: above.
    pageControlUsed = YES;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
@end
