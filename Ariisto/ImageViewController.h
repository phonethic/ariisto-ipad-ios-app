//
//  ImageViewController.h
//  Ariisto
//
//  Created by Kirti Nikam on 27/08/12.
//
//

#import <UIKit/UIKit.h>

@interface ImageViewController : UIViewController{
    NSMutableArray *imageNames;
    int pageNumber;
    int imageHeight;
    NSString *imageNameString;
}
@property (strong, nonatomic) IBOutlet UIImageView *imgView;
- (id)initWithPageNumber:(int)page imageName:(NSString *)image height:(int)scrollheight;
@end
