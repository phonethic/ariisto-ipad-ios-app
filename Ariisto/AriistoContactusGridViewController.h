//
//  AriistoContactusGridViewController.h
//  Ariisto
//
//  Created by Kirti Nikam on 13/09/12.
//
//

#import <UIKit/UIKit.h>
#import <MessageUI/MFMailComposeViewController.h>

@interface AriistoContactusGridViewController : UIViewController<MFMailComposeViewControllerDelegate>

- (IBAction)btnPerformOperation:(id)sender;

- (IBAction)btnBack:(id)sender;


@end
