//
//  AriistoAppointmentViewController.h
//  Ariisto
//
//  Created by Kirti Nikam on 31/08/12.
//
//

#import <UIKit/UIKit.h>
#import <MessageUI/MFMailComposeViewController.h>

@interface AriistoAppointmentViewController : UIViewController <UITextFieldDelegate,MFMailComposeViewControllerDelegate>

@property (strong, nonatomic) IBOutlet UITextField *textFieldName;
@property (strong, nonatomic) IBOutlet UITextField *textFieldEmail;
@property (strong, nonatomic) IBOutlet UITextField *textFieldPhone;
- (IBAction)btnBack:(id)sender;
- (IBAction)btnSend:(id)sender;

@end
