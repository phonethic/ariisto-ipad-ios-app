//
//  AriistoProjectObject.h
//  Ariisto
//
//  Created by Kirti Nikam on 03/09/12.
//
//

#import <Foundation/Foundation.h>

@interface AriistoProjectObject : NSObject
@property (nonatomic, copy) NSString *imageName;
@property (nonatomic, copy) NSString *downloadLink;
@end
