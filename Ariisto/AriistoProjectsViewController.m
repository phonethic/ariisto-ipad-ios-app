//
//  AriistoProjectsViewController.m
//  Ariisto
//
//  Created by Kirti Nikam on 31/08/12.
//
//

#import "AriistoProjectsViewController.h"

#define PROJECTS_URL @"http://192.168.254.13/ariisto_projects.xml"

NSUInteger kProjectsNumberOfPages;

@interface AriistoProjectsViewController ()
@property (assign) NSUInteger page;
@property (assign) BOOL rotating;
@end

@implementation AriistoProjectsViewController
@synthesize scrollView;
@synthesize pageControl;
@synthesize viewControllers;
@synthesize btnOperation;
@synthesize projectData;
@synthesize page = _page;
@synthesize rotating = _rotating;
@synthesize itunelink;
@synthesize  statusIndicator;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        }
    return self;
}
- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    UIViewController *viewController = [viewControllers objectAtIndex:pageControl.currentPage];
	[viewController willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    
	UIViewController *viewController = [viewControllers objectAtIndex:self.pageControl.currentPage];
	[viewController willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
    
	self.scrollView.contentSize = CGSizeMake(scrollView.frame.size.width * kProjectsNumberOfPages, scrollView.frame.size.height);
	NSUInteger page = 0;
	for (viewController in viewControllers) {
		CGRect frame = self.scrollView.frame;
		frame.origin.x = frame.size.width * page;
		frame.origin.y = 0;
		viewController.view.frame = frame;
		page++;
	}
	
	CGRect frame = self.scrollView.frame;
    frame.origin.x = frame.size.width * _page;
    frame.origin.y = 0;
	[self.scrollView scrollRectToVisible:frame animated:NO];
    
}

- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
    
	for (NSUInteger i =0; i < [viewControllers count]; i++) {
		[self loadScrollViewWithPage:i];
	}
    
	self.pageControl.currentPage = 0;
	_page = 0;
	[self.pageControl setNumberOfPages:kProjectsNumberOfPages];
    
	UIViewController *viewController = [viewControllers objectAtIndex:self.pageControl.currentPage];
	if (viewController.view.superview != nil) {
		[viewController viewWillAppear:animated];
	}
    
	self.scrollView.contentSize = CGSizeMake(scrollView.frame.size.width * kProjectsNumberOfPages, scrollView.frame.size.height);
}


- (void)viewDidAppear:(BOOL)animated {
	[super viewDidAppear:animated];
	UIViewController *viewController = [viewControllers objectAtIndex:self.pageControl.currentPage];
	if (viewController.view.superview != nil) {
		[viewController viewDidAppear:animated];
	}
}

- (IBAction)changePage:(id)sender
{
    NSLog(@"Page changed");
    int page = pageControl.currentPage;
	
	
	// update the scroll view to the appropriate page
    CGRect frame = self.scrollView.frame;
    frame.origin.x = frame.size.width * page;
    frame.origin.y = 0;
    
	UIViewController *oldViewController = [viewControllers objectAtIndex:_page];
	UIViewController *newViewController = [viewControllers objectAtIndex:self.pageControl.currentPage];
	[oldViewController viewWillDisappear:YES];
	[newViewController viewWillAppear:YES];
    
    [UIView animateWithDuration:1
                          delay:0
                        options:UIViewAnimationOptionCurveLinear
                     animations:^{ [scrollView scrollRectToVisible:frame animated:NO]; }
                     completion:NULL];
    
    //[scrollView scrollRectToVisible:frame animated:YES];
    
	// Set the boolean used when scrolls originate from the UIPageControl. See scrollViewDidScroll: above.
    pageControlUsed = YES;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    [[self navigationController] setNavigationBarHidden:YES animated:NO];
      
    imagesArray=[[NSArray alloc]initWithObjects:@"sapphire",@"cloud",@"centrum",@"chambers",@"county",@"cozy",@"glory",@"heaven",@"horizon",@"ria",@"solitaire",nil];
    kProjectsNumberOfPages=[imagesArray count];
    NSLog(@"kProjectsNumberOfPages %d",kProjectsNumberOfPages);
    
    NSMutableArray *controllers = [[NSMutableArray alloc] init];
    for (unsigned i = 0; i < kProjectsNumberOfPages; i++){
        [controllers addObject:[NSNull null]];
    }
    self.viewControllers = controllers;
    
    // a page is the width of the scroll view
    scrollView.clipsToBounds = YES;
    scrollView.scrollEnabled = YES;
    scrollView.pagingEnabled = YES;
    scrollView.contentSize = CGSizeMake(scrollView.frame.size.width * kProjectsNumberOfPages, scrollView.frame.size.height + 280);
    scrollView.showsHorizontalScrollIndicator = NO;
    scrollView.showsVerticalScrollIndicator = NO;
    scrollView.scrollsToTop = YES;
    scrollView.delegate = self;
    scrollView.bounces = NO;
    scrollView.directionalLockEnabled = YES;
    pageControl.autoresizingMask = UIViewAutoresizingFlexibleWidth ;
    
    //NSLog(@"K %d",kProjectsNumberOfPages);
    [self loadScrollViewWithPage:0];
    [self loadScrollViewWithPage:1];
    
   /* if ([ARIISTO_APP_DELEGATE networkavailable] == YES) {
        NSLog(@"Connection available :)");
        //[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        NSLog(@"Connecting ......");
        [self makeConnection:PROJECTS_URL];
    }
    else{
        NSLog(@"Connection not available :(");
        [self readDataFromFile];
    }*/
    [self makeConnection:PROJECTS_URL];
    NSLog(@"loading..");
    
}

- (void)viewWillDisappear:(BOOL)animated {
	UIViewController *viewController = [viewControllers objectAtIndex:self.pageControl.currentPage];
	if (viewController.view.superview != nil) {
		[viewController viewWillDisappear:animated];
	}
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
	UIViewController *viewController = [viewControllers objectAtIndex:self.pageControl.currentPage];
	if (viewController.view.superview != nil) {
		[viewController viewDidDisappear:animated];
	}
	[super viewDidDisappear:animated];
}

- (void)unloadScrollViewWithPage:(int)page {
    if (page < 0) return;
    if (page >= kProjectsNumberOfPages) return;
    
    UIViewController *controller = [viewControllers objectAtIndex:page];
    
    if ((NSNull *)controller != [NSNull null]) {
        if (nil != controller.view.superview) {
            [controller.view removeFromSuperview];
            controller.view=nil;
        }
        
        [viewControllers replaceObjectAtIndex:page withObject:[NSNull null]];
    }
}

- (void)viewDidUnload
{
    [self setScrollView:nil];
    [self setPageControl:nil];
    [self setBtnOperation:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)loadScrollViewWithPage:(int)page {
   // NSLog(@"check2 %d",kProjectsNumberOfPages);
    if (page < 0) return;
    if (page >= kProjectsNumberOfPages) return;
	
    // replace the placeholder if necessary
    UIViewController *controller = [viewControllers objectAtIndex:page];
    if ((NSNull *)controller == [NSNull null])
    {
       
        NSString *imageWithPrefix=[NSString stringWithFormat:@"%@.png",[imagesArray objectAtIndex:page]];
        NSLog(@"%@",imageWithPrefix);
        controller = [[ImageViewController alloc] initWithPageNumber:page imageName:imageWithPrefix height:850];        
        [viewControllers replaceObjectAtIndex:page withObject:controller];
        [btnOperation setImage:[UIImage imageNamed:@"coming-soon.png"] forState:UIControlStateNormal];
    }
    
	// add the controller's view to the scroll view
    if (controller.view.superview == nil) {
        CGRect frame = self.scrollView.frame;
        frame.origin.x = frame.size.width * page;
        frame.origin.y = 0;
        controller.view.frame = frame;
        [self.scrollView addSubview:controller.view];
    }
    
}
- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView {
	UIViewController *oldViewController = [viewControllers objectAtIndex:_page];
	UIViewController *newViewController = [viewControllers objectAtIndex:self.pageControl.currentPage];
	[oldViewController viewDidDisappear:YES];
	[newViewController viewDidAppear:YES];
    
	_page = self.pageControl.currentPage;
}


- (void)scrollViewDidScroll:(UIScrollView *)sender
{
    // We don't want a "feedback loop" between the UIPageControl and the scroll delegate in
    // which a scroll event generated from the user hitting the page control triggers updates from
    // the delegate method. We use a boolean to disable the delegate logic when the page control is used.
    if (pageControlUsed)
    {
        // do nothing - the scroll was initiated from the page control, not the user dragging
        return;
    }
	
    // Switch the indicator when more than 50% of the previous/next page is visible
    CGFloat pageWidth = scrollView.frame.size.width;
    int page = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    if(pageControl.currentPage != page) {
      [ARIISTO_APP_DELEGATE playOnScroll];
    }
    if (self.pageControl.currentPage != page) {
		UIViewController *oldViewController = [viewControllers objectAtIndex:self.pageControl.currentPage];
		UIViewController *newViewController = [viewControllers objectAtIndex:page];
		[oldViewController viewWillDisappear:YES];
		[newViewController viewWillAppear:YES];
		self.pageControl.currentPage = page;
		[oldViewController viewDidDisappear:YES];
		[newViewController viewDidAppear:YES];
		_page = page;
	}
    
}

// At the begin of scroll dragging, reset the boolean used when scrolls originate from the UIPageControl
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    pageControlUsed = NO;
    btnOperation.hidden=YES;

}

// At the end of scroll animation, reset the boolean used when scrolls originate from the UIPageControl
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    pageControlUsed = NO;
    btnOperation.hidden=NO;
    
    [self reloadData];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


- (IBAction)btnOperationMethod:(id)sender {
        
 
   // NSLog(@"page =  %d",_page);
    if(self.itunelink != nil)
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.itunelink]];
    }
    
}

//URLConnection---------------
-(void)makeConnection:(NSString *)URL{
    //NSLog(@"makeConnection");
    [statusIndicator startAnimating];
    [btnOperation setHidden:TRUE];
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:URL] cachePolicy:YES timeoutInterval:10.0];

    conn = [[NSURLConnection alloc] initWithRequest:request delegate:self startImmediately:YES];
    if (conn) {
        webData = [NSMutableData data];
    }
}

-(void) connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *) response
{
    
    [webData setLength: 0];
}

-(void) connection:(NSURLConnection *)connection didReceiveData:(NSData *) data
{
    [webData appendData:data];
    
}

-(void) connection:(NSURLConnection *)connection didFailWithError:(NSError *) error
{
    NSLog(@"didFailWithError");
    [conn cancel];
    [statusIndicator stopAnimating];
    [btnOperation setHidden:FALSE];
    [self readDataFromFile];
    
}

-(void) connectionDidFinishLoading:(NSURLConnection *) connection
{
    
    if (webData) {
        NSString *result = [[NSString alloc] initWithData:webData encoding:NSASCIIStringEncoding];
		//NSLog(@"\n result:%@\n\n", result);
        [self writeToTextFile:result name:@"projects"];
	    NSString *xmlDataFromChannelSchemes = [[NSString alloc] initWithString:result];
		NSData *xmlDataInNSData = [xmlDataFromChannelSchemes dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
		xmlParser = [[NSXMLParser alloc] initWithData:xmlDataInNSData];
		[xmlParser setDelegate:self];
		[xmlParser parse];
        }
    else{
        [self readDataFromFile];
    }
    
}

//load data in pages
-(void)reloadData{
    
    //NSLog(@"after reloading data ");
    NSString *CurrentImage=[imagesArray objectAtIndex:_page];
    for (int i =0; i<[self.projectData count]; i++)
    {
        AriistoProjectObject *prjObject=[self.projectData objectAtIndex:i];
      //  NSLog(@"CurrentImage--> %@ imageName: %@  downloadlink %@ ++++++",CurrentImage,prjObject.imageName,prjObject.downloadLink);
        if ([CurrentImage isEqualToString:prjObject.imageName])
        {
            self.itunelink=prjObject.downloadLink;
            if(prjObject.downloadLink == nil)
            {
                [btnOperation setImage:[UIImage imageNamed:@"coming-soon.png"] forState:UIControlStateNormal];
            }  else {
                [btnOperation setImage:[UIImage imageNamed:@"download.png"] forState:UIControlStateNormal];
            }
            break;
        }
    
    
    }
}


-(void) writeToTextFile:(NSString *)lcontent name:(NSString *)lname
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *fileName = [NSString stringWithFormat:@"%@.xml",lname];
    NSLog(@"filename = %@" , fileName);
    NSString* fullPath = [documentsDirectory stringByAppendingPathComponent:fileName];
    NSLog(@"fullpath = %@" , fullPath);
    
    NSError* error;
    if ([fileManager fileExistsAtPath:fullPath])
    {
        //[self removeFile:fileName];
        [lcontent writeToFile:fullPath atomically:NO encoding:NSUTF8StringEncoding error:&error];
    } else {
        [lcontent writeToFile:fullPath atomically:NO encoding:NSUTF8StringEncoding error:&error];
    }
    if(error != nil)
        NSLog(@"write error %@", error);
    else {
        NSLog(@"file created %@", fileName);
    }
    
	
}

-(NSString *) getTextFromFile:(NSString *)lname
{
	NSFileManager *fileManager = [NSFileManager defaultManager];
    //get the documents directory:
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	
	//make a file name to write the data to using the documents directory:
    NSString *fileName = [NSString stringWithFormat:@"%@.xml",lname];
    NSLog(@"filename = %@" , fileName);
    NSString* fullPath = [documentsDirectory stringByAppendingPathComponent:fileName];
    NSLog(@"fullpath = %@" , fullPath);
    NSError* error;
    
    if ([fileManager fileExistsAtPath:fullPath])
    {
        NSString *data = [NSString stringWithContentsOfFile:fullPath encoding:NSUTF8StringEncoding error:&error];
        if(error != nil)
            NSLog(@"read error %@", error);
        return data;
    }
    else
    {
        NSLog (@"Dont have net connection and running app 1st time !!");
        // check locally         
         NSString *file = [[NSBundle mainBundle] pathForResource:@"local_projects" ofType:@"xml"];  
         NSString *ldata = [NSString stringWithContentsOfFile:file encoding:NSUTF8StringEncoding error:&error];
         if(error != nil)
               NSLog(@"read error %@", error);
         return ldata;
    }
}

-(void)readDataFromFile{
  
    NSString *xmlDataFromChannelSchemes;
    NSString *data;
    data = [self getTextFromFile:@"projects"];
    //NSLog(@"\n data:%@\n\n", data);
    if(data)
    {
        xmlDataFromChannelSchemes = [[NSString alloc] initWithString:data];
    }
    
    NSData *xmlDataInNSData = [xmlDataFromChannelSchemes dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    xmlParser = [[NSXMLParser alloc] initWithData:xmlDataInNSData];
    [xmlParser setDelegate:self];
    [xmlParser parse];

}

//xml parsing----------
-(void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict{

    if ([elementName isEqualToString:@"projects"])
    {
        if(projectData == nil)
            projectData = [[NSMutableArray alloc] init];       
        
    }else if ([elementName isEqualToString:@"project"])
    {
        tempObj = [[AriistoProjectObject alloc] init];
        tempObj.imageName = [attributeDict objectForKey:@"name"];
    }
    else if ([elementName isEqualToString:@"ipad_link"]){
        elementFound = YES;
        
    }
    
    
}

-(void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string{
    

    if (elementFound){
      //  NSLog(@"found characters %@ --> %d",string,string.length);
        if (string.length == 3)
            tempObj.downloadLink=nil;    
        else
            tempObj.downloadLink=string;
    }
    elementFound = FALSE;

}

-(void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName{
    
    if ([elementName isEqualToString:@"projects"])
    {
      //  [xmlParser abortParsing];
        [statusIndicator stopAnimating];
        [btnOperation setHidden:FALSE];
        [self reloadData];
        
    }else if([elementName isEqualToString:@"project"]){        
        [self.projectData addObject:tempObj];
        tempObj=nil;
    }
}

-(void)parser:(NSXMLParser *)parser validationErrorOccurred:(NSError *)validationError{
      NSLog(@"validationErrorOccurred %@",validationError);
}

-(void)parserDidEndDocument:(NSXMLParser *)parser{
   // NSLog(@"projectData--> %@",self.projectData);
}

@end
