//
//  AriistoShowAmenityImageViewController.m
//  Ariisto
//
//  Created by Kirti Nikam on 04/09/12.
//
//

#import "AriistoShowAmenityImageViewController.h"

@interface AriistoShowAmenityImageViewController ()

@end

@implementation AriistoShowAmenityImageViewController
@synthesize imageView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (id)initWithImageName:(NSString *)name{
    if (self = [super initWithNibName:@"AriistoShowAmenityImageViewController" bundle:nil])
    {
        NSLog(@"got image as %@",name);
        imageNameString=name;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.imageView setImage:[UIImage imageNamed:imageNameString]];
}

- (void)viewDidUnload
{
    [self setImageView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}

- (IBAction)btnBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
