//
//  AriistoContactsViewController.h
//  Ariisto
//
//  Created by Kirti Nikam on 24/08/12.
//
//

#import <UIKit/UIKit.h>
#import <MessageUI/MFMailComposeViewController.h>

@interface AriistoContactsViewController : UIViewController <MFMailComposeViewControllerDelegate>

- (IBAction)btnPerformOperation:(id)sender;

@end
