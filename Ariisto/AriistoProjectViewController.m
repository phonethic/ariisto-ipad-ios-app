//
//  AriistoProjectViewController.m
//  Ariisto
//
//  Created by Kirti Nikam on 31/08/12.
//
//

#import "AriistoProjectViewController.h"
#import "AriistoCurrentProjectDetailsViewController.h"

@interface AriistoProjectViewController ()

@end

@implementation AriistoProjectViewController
@synthesize btnMore;
@synthesize projectimageView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewWillAppear:(BOOL)animated {
	// hide navigation bar
    [super viewWillAppear:animated];
	[self.navigationController setNavigationBarHidden:YES];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.projectimageView setImage:[UIImage imageNamed:@"sevenone.png"]];
    [btnMore setImage:[UIImage imageNamed:@"more.png"] forState:UIControlStateNormal];
}

- (void)viewDidUnload
{
    [self setProjectimageView:nil];
    [self setBtnMore:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}



- (IBAction)btnMore:(id)sender {
     [ARIISTO_APP_DELEGATE playOniButton];
    UIViewController *detailsController = [[AriistoCurrentProjectDetailsViewController alloc] initWithNibName:@"AriistoCurrentProjectDetailsViewController" bundle:nil] ;
    [self.navigationController pushViewController:detailsController animated:YES];
    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
@end
