//
//  AriistoPDFViewController.m
//  Ariisto
//
//  Created by Kirti Nikam on 30/08/12.
//
//

#import "AriistoPDFViewController.h"

@interface AriistoPDFViewController ()

@end

@implementation AriistoPDFViewController
@synthesize man_imageview;
@synthesize subView;
@synthesize progressView;
@synthesize webView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad]; 
 /*   NSString *resourceDocPath = [[NSString alloc] initWithString:[[[[NSBundle mainBundle]  resourcePath] stringByDeletingLastPathComponent] stringByAppendingPathComponent:@"Documents"]];
    
    filePath = [resourceDocPath stringByAppendingPathComponent:@"pdfFile.pdf"];*/
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    filePath = [documentsDirectory stringByAppendingPathComponent:@"ariisto.pdf"];
    NSLog(@"fullpath = %@" , filePath);
    NSLog(@"\n\n.....\npaths = %@" , paths);

    
    NSFileManager *filemgr= [NSFileManager defaultManager];
    if ([filemgr fileExistsAtPath: filePath] == YES)
    {
        NSLog (@"File exists");
        FLAG_BACK=TRUE;
        [self showDocument];
    }
    else
    {
        NSLog (@"File not found");
        FLAG_BACK=FALSE;
        
       
        if ([ARIISTO_APP_DELEGATE networkavailable]== YES) {
            [self.man_imageview setImage:[UIImage imageNamed:@"man_pdf.jpg"]];
            [self roundViewWithBorder:subView];
            [progressView setProgress:0];
            [self downloadDocument];
            
        }
        else{
           UIAlertView *alert = [ [ UIAlertView alloc ] initWithTitle: @"Ariisto"
                 message:@"No network is available.Please check your internet connection and try again." delegate: self cancelButtonTitle: @"OK" otherButtonTitles: nil, nil ];
                [ alert show ];
            }     
        
        
    }
   
}
-(void) roundViewWithBorder:(UIView *)view
{
    view.layer.cornerRadius = 10;
    view.clipsToBounds = YES;
    view.layer.borderColor = [UIColor lightGrayColor].CGColor;
    view.layer.borderWidth = 1.0;
}
- (void)viewDidUnload
{
    [self setWebView:nil];
    [self setProgressView:nil];
    [self setSubView:nil];
    [self setMan_imageview:nil];
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)btnBack:(id)sender {
    
    if (FLAG_BACK) {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else{
    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Alert Message"
message:@"Do you want to cancel the downloading?" delegate:self cancelButtonTitle:@"YES" otherButtonTitles:@"NO", nil];
    [alert show];
    }
}

- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSLog(@"%d", buttonIndex);
    switch (buttonIndex) {
        case 0:
            [conn cancel];
            [self.navigationController popViewControllerAnimated:YES];
            break;
        case 1:
        default:
            break;
    }

}

-(void) showDocument{
    [self.subView setHidden:YES];
    [self.man_imageview setHidden:YES];

    
   /* prevoius (viewing pdf in webview)
    NSURL *url=[NSURL fileURLWithPath:filePath];
    NSURLRequest *req=[NSURLRequest requestWithURL:url];
    webView.scrollView.bounces=NO;
    [webView setUserInteractionEnabled:YES];    
    [webView setDelegate:self];
    [webView loadRequest:req];*/
    
   //now using third party
   //localfilepath
   // NSString *file = [[NSBundle mainBundle] pathForResource:@"typo_tips" ofType:@"pdf"];
    ReaderDocument *document = [ReaderDocument withDocumentFilePath:filePath password:nil];
    if (document != nil)
    {
        ReaderViewController *readerViewController = [[ReaderViewController alloc] initWithReaderDocument:document];
        readerViewController.delegate = self;
        readerViewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        readerViewController.modalPresentationStyle = UIModalPresentationFullScreen;
        [self presentModalViewController:readerViewController animated:YES];
    }
    else
        NSLog(@"got nil");

}
- (void)dismissReaderViewController:(ReaderViewController *)viewController {
    [self dismissModalViewControllerAnimated:YES];
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)downloadDocument{
   // NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:@"https://developer.apple.com/library/ios/documentation/General/Conceptual/iCloudDesignGuide/iCloudDesignGuide.pdf"]];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:@"http://ariisto.com/pdf/Ariisto%20Sapphire%20Brochure.pdf"]];    
  
    conn = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    if (conn) {
        webData = [NSMutableData data];
    }
}

-(void) connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *) response
{    
   // NSLog(@"didReceiveResponse set webdata=0");
    NSHTTPURLResponse * httpResponse = (NSHTTPURLResponse *)response;
    contentSize = [httpResponse expectedContentLength];
    [webData setLength: 0];
}

-(void) connection:(NSURLConnection *)connection didReceiveData:(NSData *) data
{
    [webData appendData:data];
    float progress = (float)[webData length] / (float)contentSize;
   // NSLog(@"appending..progress.. %f",progress);
    [progressView setProgress:progress];
}

-(void) connection:(NSURLConnection *)connection didFailWithError:(NSError *) error
{
    NSLog(@"didFailWithError");
}

-(void) connectionDidFinishLoading:(NSURLConnection *) connection
{
  //  NSLog(@"DONE. Received Bytes: %d", [webData length]);
  //  NSLog(@"path %@",filePath);
    if (webData) {
         [self writeToTextFile:webData];
    }  

}
-(void) writeToTextFile:(NSMutableData *)lcontent
{
	
    NSLog(@"writing file with path fullpath = %@" , filePath);
    
    BOOL written=[lcontent writeToFile:filePath atomically:YES];
    if (written)
    {
        NSLog(@"Saved to file:%@", filePath);
        [self showDocument];
        FLAG_BACK=TRUE;
    }
    else
        NSLog(@"problem");   	
}
@end
