//
//  AriistoLocationViewController.h
//  Ariisto
//
//  Created by Kirti Nikam on 28/08/12.
//
//

#import <UIKit/UIKit.h>

@interface AriistoLocationViewController : UIViewController <UIScrollViewDelegate>
@property (strong, nonatomic) IBOutlet UIScrollView *locationMapScrollView;
@property (strong,nonatomic) UIImageView *imageView;
- (IBAction)btnBack:(id)sender;
@end
