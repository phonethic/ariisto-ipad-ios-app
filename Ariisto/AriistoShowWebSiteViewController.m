//
//  AriistoShowWebSiteViewController.m
//  Ariisto
//
//  Created by Kirti Nikam on 31/08/12.
//
//

#import "AriistoShowWebSiteViewController.h"

@interface AriistoShowWebSiteViewController ()

@end

@implementation AriistoShowWebSiteViewController
@synthesize webView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
 
    NSURL *url = [NSURL URLWithString:@"http://www.ariisto.com"];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
   // [webView setAllowsInlineMediaPlayback:YES];
    webView.scrollView.bounces=NO;
    [webView loadRequest:requestObj];
}

- (void)viewDidUnload
{
    [self setWebView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)btnBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)webViewDidStartLoad:(UIWebView *)webView{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}
-(void)webViewDidFinishLoad:(UIWebView *)webView1{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;

}
-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
   [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}
@end
