//
//  AriistoMyAnnotation.m
//  Ariisto
//
//  Created by Kirti Nikam on 31/08/12.
//
//

#import "AriistoMyAnnotation.h"

@implementation AriistoMyAnnotation
@synthesize coordinate;
@synthesize title;
@synthesize subtitle;

-(id)init{
    CLLocationCoordinate2D location;
    location.latitude=0;
    location.longitude=0;
    return [self initWithCoordinate:location title:nil subtitle:nil];
}

-(id)initWithCoordinate:(CLLocationCoordinate2D)c title:(NSString *)t subtitle:(NSString *)st{
    
    self=[super init];
    coordinate=c;
    title=t;
    subtitle=st;
    return self;
}
@end
