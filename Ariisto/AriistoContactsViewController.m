//
//  AriistoContactsViewController.m
//  Ariisto
//
//  Created by Kirti Nikam on 24/08/12.
//
//

#import "AriistoContactsViewController.h"
#import "AriistoShowWebSiteViewController.h"
@interface AriistoContactsViewController ()

@end

@implementation AriistoContactsViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

}
- (void)viewWillAppear:(BOOL)animated {
	// hide navigation bar
    [super viewWillAppear:animated];
	[self.navigationController setNavigationBarHidden:YES];
    
}
- (void)viewDidUnload
{
     [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


- (IBAction)btnPerformOperation:(id)sender {
    UIButton *btn=(UIButton *)sender;
    switch (btn.tag) {
        case 0:
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"You cannot Dial from this device" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:nil];
                [alert show];
            }
            break;
        case 1: //compose mail
            {
            @try{
                MFMailComposeViewController *picker=[[MFMailComposeViewController alloc]init];
                picker.mailComposeDelegate=self;
                
                [picker setToRecipients:[NSArray arrayWithObject:@"sales@ariisto.com"]];
                [picker setMessageBody:@"Enter your message here" isHTML:NO];
                [picker setModalPresentationStyle:UIModalPresentationFormSheet];
                [picker setModalTransitionStyle:UIModalTransitionStyleFlipHorizontal];
                [self presentModalViewController:picker animated:YES];
            }
            @catch (NSException *exception)
                {
                    NSLog(@"Exception %@",exception.reason);
                }
            }
            break;
        case 2: // open website
        {
            UIViewController *websiteController = [[AriistoShowWebSiteViewController alloc] initWithNibName:@"AriistoShowWebSiteViewController" bundle:nil] ;
            [self.navigationController pushViewController:websiteController animated:YES];
        }
            break;
        default:
            break;
    }
}

-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{

    [controller dismissModalViewControllerAnimated:YES];
}

@end
