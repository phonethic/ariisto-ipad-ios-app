//
//  AriistoFloorPlanViewController.m
//  Ariisto
//
//  Created by Kirti Nikam on 28/08/12.
//
//

#import "AriistoFloorPlanViewController.h"

@interface AriistoFloorPlanViewController ()

@end

@implementation AriistoFloorPlanViewController
@synthesize layoutScrollView;
@synthesize imageView;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"page_floorplan.png"]];
    imageView.frame=CGRectMake(0, 0,layoutScrollView.frame.size.width,layoutScrollView.frame.size.height);
    layoutScrollView.minimumZoomScale=1.0;
    layoutScrollView.maximumZoomScale=3.0;
    layoutScrollView.delegate=self;
    layoutScrollView.clipsToBounds=YES;
    [layoutScrollView setZoomScale:layoutScrollView.minimumZoomScale];
    [layoutScrollView addSubview:imageView];

}
-(UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView{
    return imageView;
}
- (void)viewDidUnload
{
    [self setLayoutScrollView:nil];
    [self setImageView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (IBAction)btnBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
