//
//  AriistoElevationViewController.m
//  Ariisto
//
//  Created by Kirti Nikam on 28/08/12.
//
//

#import "AriistoElevationViewController.h"

@interface AriistoElevationViewController ()

@end

@implementation AriistoElevationViewController
@synthesize scrollView;
@synthesize imageView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"page_elevation.png"]];
    imageView.frame=CGRectMake(0, 0,scrollView.frame.size.width,scrollView.frame.size.height);
    toggle=true;
    scrollView.delegate=self;
    scrollView.minimumZoomScale=1.0;
    scrollView.maximumZoomScale=3.0;
    scrollView.clipsToBounds=YES;
    [scrollView setMinimumZoomScale:scrollView.minimumZoomScale];
    [scrollView addSubview:imageView];
}

- (void)viewDidUnload
{
    [self setScrollView:nil];
    [self setImageView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView{
    return imageView;
}

- (IBAction)btnBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnToggle:(id)sender {
    UIButton *btnToggle=(UIButton *)sender;
    if (toggle) {
        [btnToggle setImage:[UIImage imageNamed:@"day.png"] forState:UIControlStateNormal];
        imageView.image = [UIImage imageNamed:@"elevation_building.png"];
        toggle=false;
    }
    else{        
        [btnToggle setImage:[UIImage imageNamed:@"night.png"] forState:UIControlStateNormal];
        imageView.image = [UIImage imageNamed:@"page_elevation.png"];
        toggle=true;
    }
}
@end
