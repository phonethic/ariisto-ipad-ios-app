//
//  AriistoMyAnnotation.h
//  Ariisto
//
//  Created by Kirti Nikam on 31/08/12.
//
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface AriistoMyAnnotation : NSObject <MKAnnotation>
@property(readonly,nonatomic) CLLocationCoordinate2D coordinate;
@property(readonly,nonatomic) NSString *title;
@property(readonly,nonatomic) NSString *subtitle;

-(id)initWithCoordinate:(CLLocationCoordinate2D) c
                  title:(NSString *)t
               subtitle:(NSString *)st;

@end
