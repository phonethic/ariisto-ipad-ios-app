//
//  AriistoLayoutPlanViewController.h
//  Ariisto
//
//  Created by Kirti Nikam on 29/08/12.
//
//

#import <UIKit/UIKit.h>

@interface AriistoLayoutPlanViewController : UIViewController<UIScrollViewDelegate>
- (IBAction)btnBack:(id)sender;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong,nonatomic) UIImageView *imageView;

@end
