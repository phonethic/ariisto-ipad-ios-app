//
//  AriistoLocationViewController.m
//  Ariisto
//
//  Created by Kirti Nikam on 28/08/12.
//
//

#import "AriistoLocationViewController.h"

@interface AriistoLocationViewController ()

@end

@implementation AriistoLocationViewController
@synthesize locationMapScrollView;
@synthesize imageView;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    //imageView.image=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"locationmap"] ofType:@"png" ]];
    
    imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"locationmap.png"]];
    imageView.frame=CGRectMake(0, 0,locationMapScrollView.frame.size.width,locationMapScrollView.frame.size.height);
    locationMapScrollView.minimumZoomScale=1.0;
    locationMapScrollView.maximumZoomScale=3.0;
    locationMapScrollView.delegate=self;
    locationMapScrollView.clipsToBounds=YES;
    [locationMapScrollView setZoomScale:locationMapScrollView.minimumZoomScale];
    [locationMapScrollView addSubview:imageView];

}
-(UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView{
    return imageView;
}

- (void)viewDidUnload
{
    [self setLocationMapScrollView:nil];
    self.imageView=nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)btnBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
