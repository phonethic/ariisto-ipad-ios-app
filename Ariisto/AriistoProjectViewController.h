//
//  AriistoProjectViewController.h
//  Ariisto
//
//  Created by Kirti Nikam on 31/08/12.
//
//

#import <UIKit/UIKit.h>
#import "AriistoAppDelegate.h"

@interface AriistoProjectViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIButton *btnMore;
@property (strong, nonatomic) IBOutlet UIImageView *projectimageView;
- (IBAction)btnMore:(id)sender;

@end
