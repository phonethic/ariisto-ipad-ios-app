//
//  AriistoHomeViewController.h
//  Ariisto
//
//  Created by Kirti Nikam on 24/08/12.
//
//

#import <UIKit/UIKit.h>
#import "AriistoAppDelegate.h"
#import <AVFoundation/AVFoundation.h>

@interface AriistoHomeViewController : UIViewController<UIScrollViewDelegate,AVAudioPlayerDelegate>{
    UIScrollView *scrollView;
    UIPageControl *pageControl;
    //AVAudioPlayer *audioPlayer;
    NSMutableArray *viewControllers;
    BOOL pageControlUsed;
    
    NSArray *homeImages;
    NSTimer *timer;
    
    AVAudioPlayer *webMusicAudioPlayer;

}
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIPageControl *pageControl;
@property (nonatomic, retain) NSMutableArray *viewControllers;
@property (nonatomic,strong) NSMutableArray *textLabelsArray;

- (IBAction)changePage:(id)sender;
- (void)loadScrollViewWithPage:(int)page ;
- (void)unloadScrollViewWithPage:(int)page ;
@end
