//
//  AriistoLayoutPlanViewController.m
//  Ariisto
//
//  Created by Kirti Nikam on 29/08/12.
//
//

#import "AriistoLayoutPlanViewController.h"

@interface AriistoLayoutPlanViewController ()

@end

@implementation AriistoLayoutPlanViewController
@synthesize scrollView;
@synthesize imageView;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"page_layoutplan.png"]];
    imageView.frame=CGRectMake(0, 0,scrollView.frame.size.width,scrollView.frame.size.height);
    scrollView.minimumZoomScale=1.0;
    scrollView.maximumZoomScale=3.0;
    scrollView.delegate=self;
    scrollView.clipsToBounds=YES;
    [scrollView setMinimumZoomScale:scrollView.minimumZoomScale];
    [scrollView addSubview:imageView];
}

- (void)viewDidUnload
{
    [self setScrollView:nil];
    self.imageView=nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}
-(UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView{
    return imageView;
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)btnBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
