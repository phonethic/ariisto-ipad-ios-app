//
//  ImageViewController.m
//  Ariisto
//
//  Created by Kirti Nikam on 27/08/12.
//
//

#import "ImageViewController.h"

@interface ImageViewController ()

@end

@implementation ImageViewController
@synthesize imgView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (id)initWithPageNumber:(int)page imageName:(NSString *)image height:(int)scrollheight {
    if (self = [super initWithNibName:@"ImageViewController" bundle:nil])
    {
        pageNumber = page;
        imageNameString=image;
        imageHeight=scrollheight;
     //   NSLog(@"----- page init = %d and scrolltype is %@ %d \n",page ,imageNameString,imageHeight);
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
  //  self.view.backgroundColor = [UIColor yellowColor];
    
  //  CGRect frame = [[UIScreen mainScreen] bounds];
  //  UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 768, 845)];
//  UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 768, imageHeight)];
    
//    switch (scrollType) {
//    
//        case 0: // Gallery
//              imageNames=[[NSMutableArray alloc] initWithObjects:@"Gallery1.jpg",@"Gallery2.jpg",@"Gallery3.jpg",@"Gallery4.jpg",
//                                @"Gallery5.jpg", @"Gallery6.jpg",@"Gallery7.jpg",@"Gallery8.jpg",
//                                @"Gallery9.jpg",@"Gallery10.jpg",@"Gallery11.jpg",@"Gallery12.jpg",
//                                @"Gallery13.jpg",@"Gallery14.jpg",@"Gallery15.jpg", nil];
//        
//            break;
//        case 1: // Internal Amenities
//            imageNames=[[NSMutableArray alloc] initWithObjects:@"InternaliPad0.jpg",@"InternaliPad1.jpg",@"InternaliPad2.jpg",
//                        @"InternaliPad3.jpg", nil];
//            
//            break;
//        case 2: //External Amenities
//            imageNames=[[NSMutableArray alloc] initWithObjects:@"ExternaliPad0.jpg",@"ExternaliPad1.jpg",@"ExternaliPad2.jpg",
//                                @"ExternaliPad3.jpg",@"ExternaliPad4.jpg",@"ExternaliPad5.jpg",
//                                @"ExternaliPad6.jpg",@"ExternaliPad7.jpg",@"ExternaliPad8.jpg", nil];
//            
//            break;
//        default: imageNames=[[NSMutableArray alloc] init];
//            break;
//    }
//            
//          ///     imgView.image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"Gallery%i",pageNumber+1] ofType:@"jpg" ]];
//    NSLog(@"Images %@",imageNames);
//     imgView.image = [UIImage imageNamed:[imageNames objectAtIndex:pageNumber]];
//	
    imgView.image = [UIImage imageNamed:imageNameString];
    [imgView setContentMode:UIViewContentModeScaleToFill];
    [self.view addSubview:imgView];
}
- (void)viewDidUnload
{
    [self setImgView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


@end
