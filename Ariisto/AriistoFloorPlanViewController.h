//
//  AriistoFloorPlanViewController.h
//  Ariisto
//
//  Created by Kirti Nikam on 28/08/12.
//
//

#import <UIKit/UIKit.h>

@interface AriistoFloorPlanViewController : UIViewController<UIScrollViewDelegate>
@property (strong, nonatomic) IBOutlet UIScrollView *layoutScrollView;
@property (strong,nonatomic) UIImageView *imageView;

- (IBAction)btnBack:(id)sender;

@end
